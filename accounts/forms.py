from django import forms
from django.contrib.auth.models import User
from projects.models import Project


class LoginForm(forms.Form):
    username = forms.CharField(max_length=150, label="username")
    password = forms.CharField(
        max_length=150, widget=forms.PasswordInput, label="password"
    )


class SignUpForm(forms.Form):
    username = forms.CharField(max_length=150)
    password = forms.CharField(
        max_length=150,
        widget=forms.PasswordInput,
    )
    password_confirmation = forms.CharField(
        max_length=150,
        widget=forms.PasswordInput,
    )

    class Meta:
        model = User
        fields = ["username"]


def clean(self):
    cleaned_data = super(self).clean()
    password = cleaned_data.get("password")
    password_confirmation = cleaned_data.get("password_confirmation")

    if (
        password
        and password_confirmation
        and password != password_confirmation
    ):
        self.add_error("password_confirmation", "The passwords do not match")
    return cleaned_data
